import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:tflite/tflite.dart';
import 'dart:math' as math;

import 'camera.dart';
import 'bndbox.dart';


class MenuPage extends StatefulWidget {
  @override
  _MenuPageState createState() => new _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  List<dynamic> _recognitions;
  int _imageHeight = 0;
  int _imageWidth = 0;
  String _model = "";
  List<CameraDescription> cameras;
  bool voiceEnable = false;

  FlutterTts flutterTts = FlutterTts();

  configSpeak() async{
    await flutterTts.setLanguage("pt-BR");
    await flutterTts.setVoice({"name": "Karen", "locale": "pt-BR"});
    await flutterTts.setSpeechRate(1.0);
    await flutterTts.setVolume(1.0);
    await flutterTts.setPitch(1.0); 
    await flutterTts.setQueueMode(1);
  }
  

  @override
  void initState() {
    super.initState();
    availableCameras().then((value){
      cameras = value;
    });
  }

  _speak(text) {
     flutterTts.awaitSpeakCompletion(true);
     flutterTts.speak(text);
  }

  loadModel() async {
    String res;
    try {
      res = await Tflite.loadModel(
        model: "assets/model_unquant.tflite",
        labels: "assets/labels.txt");
    } catch (e) {
      print(e);
    }

    print(res);
    print(res);
  }

  onSelect() {
    setState(() {
      _model = 'teste';
    });
    loadModel();
  }

  setRecognitions(recognitions, imageHeight, imageWidth) {

    if(voiceEnable) for (var item in recognitions) {
      print(item);
       _speak(item['label'].toString().split(" ")[1]);

    }

    setState(() {
      _recognitions = recognitions;
      _imageHeight = imageHeight;
      _imageWidth = imageWidth;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size screen = MediaQuery.of(context).size;
    return Scaffold(
       appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              setState(() {
                _model = "";
              });
            }),
      ),
      bottomNavigationBar: _model == "" ? Container(
        height: 0, 
        width: 0, 
        color: Colors.transparent,) : BottomAppBar(
        color: Colors.transparent,
        elevation: 0,
        child: Container(
          margin: EdgeInsets.only(bottom: 10, top: 10),
          color: Colors.transparent,
          child: ListTile(
            title: Text(voiceEnable ? "Desabilitar voz" : "Habilitar voz", style: TextStyle(color: Colors.white, fontSize: 22, fontWeight: FontWeight.w300),),
            trailing: voiceEnable ? Icon(LineAwesomeIcons.microphone_slash, color: Colors.white, size: 30.0,) : Icon(LineAwesomeIcons.microphone, color: Colors.white, size: 30.0,),
            onTap: () {
              voiceEnable = !voiceEnable;
            },
          ),
        )
        
      ),
      backgroundColor: Colors.white,
      body: WillPopScope(
        onWillPop: () async{
          setState(() {
            _model = "";
          });
          return null;
        },
        child: _model == ""
          ? Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 100,
                  width: 100,
                  child: FlareActor("assets/home.flr", alignment:Alignment.center, fit:BoxFit.contain, animation:"Aura"),
                ),
                Container(
                  height: 20,
                ),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    "PLL",
                    style: TextStyle(color: Colors.black, fontSize: 25),
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  height: 30,
                ),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    "Tela Quebrada: 300 imagens treinadas",
                    style: TextStyle(color: Colors.black, fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    "Tela Normal: 198 imagens treinadas",
                    style: TextStyle(color: Colors.black, fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  height: 30,
                ),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    "Total: 498 imagens treinadas",
                    style: TextStyle(color: Colors.black, fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  height: 80,
                ),
                TextButton(
                  style: TextButton.styleFrom(
                    primary: Colors.yellow,
                    backgroundColor: Colors.black,
                    fixedSize: Size(MediaQuery.of(context).size.width * 0.6, MediaQuery.of(context).size.height * 0.09),
                  ),
                  child: Text(
                    "INICIAR",
                    style: TextStyle(color: Colors.white, fontSize: 22),
                  ),
                  onPressed: () => onSelect(),
                ),
              ],
            ),
          )
        : Stack(
            children: [
              Camera(
                cameras,
                setRecognitions,
              ),

              BndBox(
                _recognitions == null ? [] : _recognitions,
                math.max(_imageHeight, _imageWidth),
                math.min(_imageHeight, _imageWidth),
                screen.height,
                screen.width,
                flutterTts,
                voiceEnable
              ),
            ],
          ),
      ),
    );
  }
}
