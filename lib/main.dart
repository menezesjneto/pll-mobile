import 'dart:async';
import 'package:flutter/material.dart';
import 'menu.dart';



Future<Null> main() async {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.white,
        appBarTheme: AppBarTheme(
          color: Colors.white
        )
      ),
      title: 'PLL',
      home: MenuPage(),
    );
  }
}
