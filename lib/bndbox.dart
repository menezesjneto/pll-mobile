import 'package:flutter/material.dart';

class BndBox extends StatelessWidget {
  final List<dynamic> results;
  final int previewH;
  final int previewW;
  final double screenH;
  final double screenW;
  final flutterTts;
  bool voiceEnable;

  BndBox(
    this.results,
    this.previewH,
    this.previewW,
    this.screenH,
    this.screenW,
    this.flutterTts,
    this.voiceEnable,
  );

  @override
  Widget build(BuildContext context) {

    List<Widget> _renderBox() {
      return results.map((re) {
        return re['confidence'] > 0.9 ? Container(
          height: MediaQuery.of(context).size.height,
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(re['label'].toString(), style: TextStyle(
                color: Colors.white,
                fontSize: 30.0,
                fontWeight: FontWeight.bold,
              )),
              SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Certeza: ", style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w300,
                  )),
                  Text((re['confidence'] * 100 ).toStringAsFixed(2) + '%', style: TextStyle(
                    color: Colors.green,
                    fontSize: 25.0,
                    fontWeight: FontWeight.bold,
                  )),
                ],
              )
            ],
          ),
        ) : Container();
      }).toList();
    }

    return Stack(
      children: _renderBox(),
    );
  }
}